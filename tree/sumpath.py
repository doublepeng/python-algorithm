#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: pengchao
from  tree import *

class fTreeNode(TreeNode):
    def __cmp__(self,other):
        if self.data == other.data:
            return 0
        elif self.data < other.data:
            return -1
        else:
            return 1

class fTree(Tree): 
    
    def __init__(self,values=None):
        self.root=None
        self.values=values
        if values:
            self.root=self.create(values,1)

    def create(self,values,position=1):
        """
        递归建立二叉树
        """
        if position > len(values):
            return None
        newNode = fTreeNode(values[position-1])
        newNode.left=self.create(values,position*2)
        newNode.right=self.create(values,position*2+1)

        return newNode
    def _getpath(self,value,node,path=[]):
        
        if not node:
            return
        path.append(node.data)

        #获取路径
        topValue=node.data
        if node.left is None and node.right is None:
            if value == topValue:
                print path
        
        #递归
        self._getpath(value-topValue,node.left,path)
        self._getpath(value-topValue,node.right,path)

        path.pop()
    def getPath(self,value):
        return self._getpath(value,self.root)

if __name__=='__main__':
    
    values=[10,5,12,4,7]
    ftree=fTree(values)
    
    print ftree.getPath(22)

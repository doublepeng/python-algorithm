#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: pengchao

class TreeNode(object):
    def __init__(self,data=None,left=None,right=None):
        self.data=data
        self.left=left
        self.right=right
#二叉树定义及遍历算法实现
class Tree(object):
    def __init__(self,root=None):
        self.root=None

    def makeTree(self,data,left,right):
        self.root=TreeNode(data,left,right)

    def is_empty(self):
        if self.root is None:
            return True
        else:
            return False
    def preOrder(self,r):
        """前序遍历"""
        if not r.is_empty():
            print r.root.data,
        if r.root.left is not None:
            r.preOrder(r.root.left)
        if r.root.right is not None:
            r.preOrder(r.root.right)

    def inOrder(self,r):
        """中序遍历""" 
        if r.root.left is not None:
            r.preOrder(r.root.left)
        if not r.is_empty():
            print r.root.data,
        if r.root.right is not None:
            r.preOrder(r.root.right)

    def postOrder(self,r):
        """后序遍历"""  
        if r.root.left is not None:
            r.preOrder(r.root.left)
        if r.root.right is not None:
            r.preOrder(r.root.right)
        if not r.is_empty():
            print r.root.data,

    def levelOrder(self,r):
        """层次遍历"""
        if not r.is_empty():
            s=[r,]
            while len(s)>0:
                temp=s.pop(0) #先弹出新append
                if temp and temp.root is not None:
                    print temp.root.data,
                    if temp.root.left is not None:
                        s.append(temp.root.left)
                    if temp.root.right is not None:
                        s.append(temp.root.right)
if __name__=='__main__':
    ra, rb, rc, rd, re, rf = Tree(), Tree(), Tree(), Tree(), Tree(), Tree()
    ra.makeTree("a", None, None)
    rb.makeTree("b", None, None)
    rc.makeTree("c", None, None)
    rd.makeTree("d", None, None)
    re.makeTree("e", None, None)
    rf.makeTree("f", None, None)
    r1, r2, r3, r4, r = Tree(), Tree(), Tree(), Tree(), Tree()
    r1.makeTree("-", rc, rd)
    r2.makeTree("*", rb, r1)
    r3.makeTree("+", ra, r2)
    r4.makeTree("/", re, rf)
    r.makeTree("-", r3, r4)
    print u"前序遍历"
    r.preOrder(r)
    print u"\n中序遍历"
    r.inOrder(r)
    print u"\n后序遍历"
    r.postOrder(r)
    print u"\n层次遍历"
    r.levelOrder(r)

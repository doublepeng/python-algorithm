#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: pengchao
import random


def maxArray(ar):
    currentSum = 0
    maxSum = 0
    for a in ar:
        currentSum += a
        if currentSum < 0:
            currentSum = 0
        elif currentSum > maxSum:
            maxSum = currentSum
    if maxSum == 0:  # 说明一直都是负数
        return min(maxSum, max(ar))
    else:
        return maxSum
if __name__ == '__main__':
    ranAr = map(lambda x: x - 5, range(10))
    ar = [random.choice(ranAr) for i in range(10)]
    print ar
    print maxArray(ar)

#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: pengchao
import random


def oddeven(ar):
    begin = 0
    end = len(ar) - 1
    while (begin < end):
        while(ar[begin] % 2 == 1 and begin < end):
            begin += 1
        while(ar[end] % 2 == 0 and begin < end):
            end -= 1
        ar[begin], ar[end] = ar[end], ar[begin]
    return ar
if __name__ == '__main__':
    ranAr = map(lambda x: x + 1, range(10))
    ar = [random.choice(ranAr) for i in range(10)]
    print oddeven(ar)

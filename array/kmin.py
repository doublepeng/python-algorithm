#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: pengchao

import random


def kmin(ar, k):
    if k > len(ar):
        return False
    kar = ar[:k]
    remainArr = ar[k:]
    for r in remainArr:
        maxKar = max(kar)
        if r < max(kar):
            kar.remove(maxKar)
            kar.append(r)
    return kar

if __name__ == '__main__':
    ranAr = map(lambda x: x - 5, range(10))
    ar = [random.choice(ranAr) for i in range(10)]
    k = random.randint(1, 10)
    kar = kmin(ar, k)
    if kar == False:
        print u"错误"
    else:
        print u"原始10个元素数组", ar
        print u"最小%2d个元素数组" % k, kar

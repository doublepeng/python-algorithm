#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: pengchao
import random


def ramin(ar):
    arlen = len(ar)
    index1 = 0
    index2 = arlen - 1
    indexmid = index1
    while(ar[index1] >= ar[index2]):
        if index2 - index1 == 1:
            return ar[index2]
        if ar[index1] == ar[index2] == ar[indexmid]:
            return min(ar)
        indexmid = (index1 + index2) / 2
        if ar[indexmid] >= ar[index1]:
            index1 = indexmid
        elif ar[index2] >= ar[indexmid]:
            index2 = indexmid
    return ar[indexmid]
if __name__ == '__main__':
    k = random.randint(20, 100)
    ranAr = map(lambda x: x + k, range(k))
    kbegin = random.randint(1, 20)
    ar = ranAr[k - kbegin:] + ranAr[:kbegin]
    print ar
    print ramin(ar)

